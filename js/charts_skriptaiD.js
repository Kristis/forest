
     
	  
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);


// Draw the chart and set the chart values
 
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Percentage of population'],
  ['Football', 4000000000],
  ['Basketball', 1000000000],
  ['Cricket', 2000000000],
  ['Tennis', 500000000],
  ['Other', 500000000],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'World’s Most Popular Sports', 'width':440, 'height':300, 'backgroundColor': 'transparent',};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
  
    var data = google.visualization.arrayToDataTable([
  ['Task', 'Pieces'],
  ['Tam davė', 1],
  ['Tam davė', 1],
  ['Tam davė', 1],
  ['Tam davė', 1],
  ['O tam nebeliko', 1,],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Perfect pie cut', 'width':440, 'height':300, 'backgroundColor': 'transparent',};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
  chart.draw(data, options);
  
    var data = google.visualization.arrayToDataTable([
  ['Task', 'Pcs per pcs'],
  ['Pusė', 50 ],
  ['Ketvertuks', 25],
  ['Kits ketvertuks', 25],
  
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Pusę per pusę', 'width':440, 'height':300, 'backgroundColor': 'transparent',};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
  chart.draw(data, options); 
  
}



// stulpelinė diagrama

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"],
        datasets: [{
            label: 'Average points for Donatas Motiejūnas',
            data: [4, 19, 6, 12, 2, 3, 20, 25],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
				'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
				'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});