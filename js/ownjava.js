
     
	  
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);


// Draw the chart and set the chart values
 
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Tennis', 5],
  ['Tennis', 10],
  ['Tennis', 7],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Average Day of Serena', 'width':400, 'height':300, 'backgroundColor': '#f2f2f2',};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
  chart.draw(data, options);
  
    var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Tennis', 10],
  ['Table Tennis', 2],
  ['Squash', 7],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Hobbies of Serena', 'width':400, 'height':300, 'backgroundColor': '#f2f2f2',};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
  chart.draw(data, options);
  
    var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Victories', 99 ],
  ['Losts', 1],
  
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Carrier of Serena', 'width':400, 'height':300, 'backgroundColor': '#f2f2f2',};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
  
  
}




     google.charts.load('current', {'packages':['corechart', 'bar']});
      google.charts.setOnLoadCallback(drawStuff);
function drawStuff() {

        var button = document.getElementById('change-chart');
        var chartDiv = document.getElementById('chart_div');

        var data = google.visualization.arrayToDataTable([
          ['Season', 'Distance', 'Speed'],
          ['2013', 50, 50.3],
          ['2014', 45, 45.5],
          ['2015', 42, 39.3],
          ['2016', 39, 25.9],
          ['2017', 43, 69.1]
        ]);

        var materialOptions = {
          width: 900,
          chart: {
            title: 'Hit of Serena',
            subtitle: 'distance on the left, speed on the right'
          },
          series: {
            0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
            1: { axis: 'speed' } // Bind series 1 to an axis named 'brightness'.
          },
          axes: {
            y: {
              distance: {label: 'meters'}, // Left y-axis.
              speed: {side: 'right', label: 'm/s'} // Right y-axis.
            }
          }
        };

        var classicOptions = {
          width: 900,
          series: {
            0: {targetAxisIndex: 0},
            1: {targetAxisIndex: 1}
          },
          title: 'Hit of Serena - distance on the left, speed on the right',
          vAxes: {
            // Adds titles to each axis.
            0: {title: 'meters'},
            1: {title: 'm/s'}
          }
        };

        function drawMaterialChart() {
          var materialChart = new google.charts.Bar(chartDiv);
          materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
          button.innerText = 'Change to Classic';
          button.onclick = drawClassicChart;
        }

        function drawClassicChart() {
          var classicChart = new google.visualization.ColumnChart(chartDiv);
          classicChart.draw(data, classicOptions,);
          button.innerText = 'Change to Material';
          button.onclick = drawMaterialChart;
        }

        drawMaterialChart();
    };

function openTab(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}

// function myMap() {
    // var mapOptions = {
        // center: new google.maps.LatLng(54.710559, 25.286894),
        // zoom: 10,
        // mapTypeId: google.maps.MapTypeId.HYBRID
    // }
// var map = new google.maps.Map(document.getElementById("map"), mapOptions);
// }


// google'o žemėlapis
 function initMap() {
        var uluru = {lat: 54.711240, lng: 25.286854};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }